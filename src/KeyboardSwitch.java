
//        Создайте класс KeyboardSwitch. Его конструктор принимает строку и внутри
//        него происходит: Перевод текста в нужный язык(забыл переключить клавиатуру
//                и напечатал другим языком).
//                Критерии оценивания:

//       A=65
//       Z=90
//       a=97
//       z=122

//       А=1040
//       Я=1071
//       а=1072
//       я=1103

public class KeyboardSwitch {
    String str=new String();
    private char[] en={'q','w','e','r','t','y','u','i','o','p','[',']','a','s','d','f','g','h','j','k','l',';','\'','z','x','c','v','b','n','m',',','.','/'};
    private char[] EN={'Q','W','E','R','T','Y','U','I','O','P','{','}','A','S','D','F','G','H','J','K','L',':','\"','Z','X','C','V','B','N','M','<','>','?'};

    private char[] rus={'й','ц','у','к','е','н','г','ш','щ','з','х','ъ','ф','ы','в','а','п','р','о','л','д','ж','э','я','ч','с','м','и','т','ь','б','ю','.'};
    private char[] RUS={'Й','Ц','У','К','Е','Н','Г','Ш','Щ','З','Х','Ъ','Ф','Ы','В','А','П','Р','О','Л','Д','Ж','Э','Я','Ч','С','М','И','Т','Ь','Б','Ю',','};
    KeyboardSwitch(String str) throws Exception {
        StringBuilder tmp=new StringBuilder();
        int index=-1;
        if (isRus(str.charAt(0)))
        {
            for (int i=0;i<str.length();i++){
                if (this.isRus(str.charAt(i))){
                    index=this.cheakCharInRefactor(str.charAt(i),rus);
                    if (index>-1){
                        tmp.append(this.en[index]);
                    } else if (index==-1) {
                        index = this.cheakCharInRefactor(str.charAt(i), RUS);
                        if (index==-1) throw new Exception("Неизвестно что вы имели ввиду под этим символом ("+str.charAt(i)+") в строке "+str+" под номером "+i);
                        tmp.append(this.EN[index]);
                    }
                }
                else
                if (!this.isRus(str.charAt(i))){
                    index=this.cheakCharInRefactor(str.charAt(i),en);
                    if (index>-1){
                        tmp.append(this.rus[index]);
                    } else if (index==-1) {
                        index = this.cheakCharInRefactor(str.charAt(i), EN);
                        if (index==-1) throw new Exception("Неизвестно что вы имели ввиду под этим символом ("+str.charAt(i)+") в строке "+str+" под номером "+i);
                        tmp.append(this.RUS[index]);
                    }
                }

            }
            this.str=tmp.toString();

            System.out.println(this.str);
        }

    }


    private boolean isRus(char ch){
        if (ch>=1040&&ch<=1103) return true;

        return false;
    }



    //если возвращаем -1 значит символ не найден
    private int cheakCharInRefactor(char ch, char[] arrChar){
        for (int i=0;i<arrChar.length;i++)
            if (ch==arrChar[i]) {
                return i;
            }

        return -1;
    };


}

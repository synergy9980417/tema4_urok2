public class MyWebSite {
    String domen;
    String about;
    String framework;

    MyWebSite(String domen){
        this.domen=domen;

        System.out.println("создан объект MyWebSite "+this.domen);
    };

    MyWebSite(String domen, String about){
        this.domen=domen;
        this.about=about;
        System.out.println("создан объект MyWebSite "+this.domen+" "+this.about);
    };

    MyWebSite(String domen, String about, String framework){
        this.domen=domen;
        this.about=about;
        this.framework=framework;

        System.out.println("создан объект MyWebSite "+this.domen+" "+this.about+" "+this.framework);
    };

}

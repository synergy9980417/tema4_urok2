
//        Создайте класс TextChecker. Его конструктор принимает строку и внутри него
//        будет происходить: Проверка текста на наличие инностранных букв вместо
//        русской раскладки и цифр вместо нуля и буквы «з». Если обнаруж
//        ена такая
//        подмена


public class TextChecker {
    //создаём два массива, один содержит неправильный символ, второй массив содержит правильный символ, с одинаковыми индексами в массивах!!!
    private char[] forRefactor={'a','A','c','C','B','e','E','T','M','k','K','H','o','O','3','0'};
    private char[] rightSimvol={'а','А','с','С','В','е','Е','Т','М','к','К','Н','о','О','З','О'};
    String str=new String();

    TextChecker(String str) throws Exception {
        int tmp;
        StringBuilder newstr=new StringBuilder();
        for (int i = 0; i < str.length(); i++)
        {
            tmp=this.cheakCharInRefactor(str.charAt(i));
            if (tmp>=0) {
                System.err.println("найден символ на замену, с ("+str.charAt(i)+") на ("+this.rightSimvol[tmp]+") в строке \""+str+"\" это символ номер "+i );
                newstr.append(this.rightSimvol[tmp]);
                System.out.println("символ заменяем");
            }
            else if (this.chkChar(str.charAt(i))) {
                newstr.append(str.charAt(i));
            } else throw new Exception("Неизвестно что вы имели ввиду под этим символом ("+str.charAt(i)+") в строке "+str+" под номером "+i);
        }
        this.str = newstr.toString();
    }

//если возвращаем -1 значит символ не найден в массиве неправильных символов,  в остальных случаях получаем индекс
    private int cheakCharInRefactor(char ch){
        for (int i=0;i<this.forRefactor.length;i++)
            if (ch==this.forRefactor[i]) {
                return i;

            }

        return -1;
    };


    private boolean chkChar(char ch){
//       A=65
//       Z=90
//       a=97
//       z=122

//       А=1040
//       Я=1071
//       а=1072
//       я=1103
        if ((ch>=1040&&ch<=1103)) return true;
        return false;
    }



}
